﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;
using UnityEngine;

public struct LevelStruct
{
    public float timeLimit;
    public float itemSpeed;
    public int totalMin;
    public int totalMax;
    public int fastMin;
    public int fastMax;
    public float bombProbability;
}

public class GameManager : Singleton<GameManager>
{
    public TextAsset xmlRawFile;

    public PrepareGameInterfaceCanvas prepareGameInterfacePrefab;
    private PrepareGameInterfaceCanvas _prepareGameInterface;

    public GameInterfaceCanvas gameInterfacePrefab;
    private GameInterfaceCanvas _gameInterface;

    public GameOverLoseCanvas gameOverLoseCanvasPrefab;
    private GameOverLoseCanvas _gameOverLoseCanvas;

    public GameOverWinCanvas gameOverWinCanvasPrefab;
    private GameOverWinCanvas _gameOverWinCanvas;

    public ItemsGenerator itemsGenerator;
    public GameBasketContainer gameBasketContainer;
    public BottomTrap bottomTrap;

    private float _paramsTimeLimit;
    private int _paramsGoalsMin;
    private int _paramsGoalsMax;
    private int _paramsSteps;
    private List<LevelStruct> _levels = new List<LevelStruct>();

    public int eggsGenerated;
    public int eggsCaught;

    private bool _isPlaying = false;
    private int _gameGoals;
    private float _gameDisplayTimer;
    private float _gameInternalTime;
    private int _gameCurrentDifficulty;

    /***********************************************************
     * 
     **********************************************************/
    public int CountdownTime
    {
        get { return (int)(_paramsTimeLimit - _gameDisplayTimer); }
    }

    /***********************************************************
    * 
    **********************************************************/
    public int TargetItemsCount
    {
        get { return _gameGoals; }
    }

    /***********************************************************
    * 
    **********************************************************/
    public void setIsPlaying(bool value)
    {
        _isPlaying = value;
        itemsGenerator.isPlaying = _isPlaying;
        gameBasketContainer.SetIsPlaying(_isPlaying);
    }

    /***********************************************************
    * 
    **********************************************************/
    private void Awake()
    {
        string data = xmlRawFile.text;
        ParseXmlFile(data);

    }

    // Start is called before the first frame update
    /***********************************************************
     * 
     **********************************************************/
    void Start()
    {
        Reset();
        PrepareStartGame();
    }

    /***********************************************************
     * 
     **********************************************************/
    void Update()
    {
        if (_isPlaying)
        {
            _gameDisplayTimer += Time.deltaTime * Time.timeScale;
            _gameInternalTime += Time.deltaTime * Time.timeScale;
            if (_gameDisplayTimer >= _paramsTimeLimit) {
                GameOverLose(0);
            }
            
            int detectedDifficulty = 0;
            for (int i = 0; i < _levels.Count; i++) {
                if (_gameInternalTime > _levels[i].timeLimit)
                {
                    detectedDifficulty = i;
                }
                else {
                    break;
                }
            }
            if (_gameCurrentDifficulty != detectedDifficulty) {
                setActiveDifficulty(detectedDifficulty);
            }
        }
    }

    /***********************************************************
     * 
     **********************************************************/
    void setActiveDifficulty(int difficulty) {
        _gameCurrentDifficulty = difficulty;

        if (itemsGenerator)
        {
            itemsGenerator.itemSpeed = _levels[_gameCurrentDifficulty].itemSpeed;

            itemsGenerator.maxActiveItems = _levels[_gameCurrentDifficulty].totalMax;
            itemsGenerator.minActiveItems = _levels[_gameCurrentDifficulty].totalMin;

            itemsGenerator.minActiveFastItems = _levels[_gameCurrentDifficulty].fastMin;
            itemsGenerator.maxActiveFastItems = _levels[_gameCurrentDifficulty].fastMax;

            itemsGenerator.bombProbability = _levels[_gameCurrentDifficulty].bombProbability;

            itemsGenerator.minBombsItems = 1;
            itemsGenerator.maxBombsItems = 2;
        }
    }


    /***********************************************************
    * 
    **********************************************************/
    public void AddGeneratedEgg() {
        if (!_isPlaying) {
            return;
        }
        eggsGenerated += 1;
    }

    /***********************************************************
    * 
    **********************************************************/
    public void ItemInBasket(ItemPrototype item)
    {
        if (!_isPlaying)
        {
            return;
        }
        itemsGenerator.RemoveItem(item);
        if (item.Object == ItemPrototype.ObjectType.Bomb)
        {
            GameOverLose(0);
            return;
        }
        eggsCaught += 1;
        if (eggsCaught >= _gameGoals) {
            eggsCaught = _gameGoals;
            GameOverWin(0);
        }
    }

    /***********************************************************
     * 
     **********************************************************/
    public void ItemInTrap(ItemPrototype item)
    {
        if (!_isPlaying)
        {
            return;
        }
        itemsGenerator.RemoveItem(item);
        if (item.Object == ItemPrototype.ObjectType.Egg)
        {
            _gameDisplayTimer += 5;
            _gameInterface.TimeDecrementAnimation();
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    void PrepareStartGame() {
        if (_prepareGameInterface) {
            return;
        }
        _prepareGameInterface = (PrepareGameInterfaceCanvas)Instantiate(
            prepareGameInterfacePrefab,
            new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
            Quaternion.identity);
        _prepareGameInterface.transform.SetParent(this.transform.parent, true);
        _prepareGameInterface.StartCounter(3);
    }

    /***********************************************************
    * 
    **********************************************************/
    public void StartGame()
    {
        Destroy(_prepareGameInterface.gameObject);
        _prepareGameInterface = null;

        _gameInterface = (GameInterfaceCanvas)Instantiate(
            gameInterfacePrefab,
            new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
            Quaternion.identity);
        _gameInterface.transform.SetParent(this.transform.parent, true);

        Reset();
        setIsPlaying(true);
        BackgroundMusicOnScene.instance.Play();
        gameBasketContainer.Show();
    }

    /***********************************************************
    * 
    **********************************************************/
    void GameOverWin(int state)
    {
        if (state == 0)
        {
            BackgroundMusicOnScene.instance.Stop();
            itemsGenerator.RemoveAllItems();
            Destroy(_gameInterface.gameObject);
            setIsPlaying(false);
            StartCoroutine("CallGameOverWinAfterPause");
            return;
        }
        if (_gameOverWinCanvas) {
            return;
        }
        gameBasketContainer.Hide();
        _gameOverWinCanvas = (GameOverWinCanvas)Instantiate(
            gameOverWinCanvasPrefab,
            new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
            Quaternion.identity);
        _gameOverWinCanvas.transform.SetParent(this.transform.parent, true);
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator CallGameOverWinAfterPause()
    {
        yield return new WaitForSeconds(1.0f);
        GameOverWin(1);
    }

    /***********************************************************
     * 
     **********************************************************/
    void GameOverLose(int state) {
        if (state == 0)
        {
            BackgroundMusicOnScene.instance.Stop();
            itemsGenerator.RemoveAllItems();
            Destroy(_gameInterface.gameObject);
            setIsPlaying(false);
            StartCoroutine("CallGameOverLoseAfterPause");
            return;
        }
        if (_gameOverLoseCanvas)
        {
            return;
        }
        gameBasketContainer.Hide();
        _gameOverLoseCanvas = (GameOverLoseCanvas)Instantiate(
            gameOverLoseCanvasPrefab,
            new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
            Quaternion.identity);
        _gameOverLoseCanvas.transform.SetParent(this.transform.parent, true);
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator CallGameOverLoseAfterPause()
    {
        yield return new WaitForSeconds(1.0f);
        GameOverLose(1);
    }

    // Update is called once per frame
    /***********************************************************
     * 
     **********************************************************/
    public void Reset()
    {
        setIsPlaying(false);
        BackgroundMusicOnScene.instance.Stop();
        _gameGoals = UnityEngine.Random.Range(_paramsGoalsMin, _paramsGoalsMax + 1);
        _gameInternalTime = 0;
        _gameDisplayTimer = 0;
        eggsGenerated = 0;
        eggsCaught = 0;
        setActiveDifficulty(0);
    }

    /***********************************************************
    * 
    **********************************************************/
    void ParseXmlFile(string xmlData)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));

        ParseXmlGlobalSettings(xmlDoc.SelectSingleNode("//difficulty/global"));

        for (int i = 1; i <= _paramsSteps; i++) {
            string xmlLevelPath = string.Format("//difficulty/levels/level{0}", i);
            ParseXmlLevels(xmlDoc.SelectSingleNode(xmlLevelPath), i);
        }
     }

    /***********************************************************
    * 
    **********************************************************/
    void ParseXmlGlobalSettings(XmlNode rootNode) {
        _paramsGoalsMin = int.Parse(rootNode.SelectSingleNode("goalsMin").InnerText);
        _paramsGoalsMax = int.Parse(rootNode.SelectSingleNode("goalsMax").InnerText);
        _paramsTimeLimit = float.Parse(rootNode.SelectSingleNode("timeLimit").InnerText);
        _paramsSteps = int.Parse(rootNode.SelectSingleNode("steps").InnerText);
    }

    /***********************************************************
    * 
    **********************************************************/
    void ParseXmlLevels(XmlNode rootNode, int stepId)
    {
        LevelStruct newLevel = new LevelStruct();
        newLevel.timeLimit = float.Parse(rootNode.SelectSingleNode("timeLimit").InnerText);
        newLevel.itemSpeed = float.Parse(rootNode.SelectSingleNode("itemSpeed").InnerText);
        newLevel.totalMin = int.Parse(rootNode.SelectSingleNode("totalMin").InnerText);
        newLevel.totalMax = int.Parse(rootNode.SelectSingleNode("totalMax").InnerText);
        newLevel.fastMin = int.Parse(rootNode.SelectSingleNode("fastMin").InnerText);
        newLevel.fastMax = int.Parse(rootNode.SelectSingleNode("fastMax").InnerText);
        newLevel.bombProbability = float.Parse(rootNode.SelectSingleNode("bombProbability").InnerText);
        _levels.Add(newLevel);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrepareGameInterfaceCanvas : MonoBehaviour
{
    public Text labelText;
    private int _countdownTimer = 0;

    private AudioSource _audioSource;
    public AudioClip soundTick;
    public AudioClip soundGo;

    private float _delayToNextTick = 1.0f;

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    /***********************************************************
     * 
     **********************************************************/
    void Update()
    {
        
    }

    /***********************************************************
    * 
    **********************************************************/
    public void StartCounter(int val) {
        _countdownTimer = val;

        if (_audioSource && soundTick)
        {
            _audioSource.PlayOneShot(soundTick, 1.0f);
        }
        UpdateLabel(string.Format("{0}", _countdownTimer));
    }

    /***********************************************************
    * 
    **********************************************************/
    void LaunchGame() {
        labelText.text = "";
        GameManager.instance.StartGame();
    }

    /***********************************************************
    * 
    **********************************************************/
    void UpdateLabel(string text) {
        labelText.text = text;
        StartCoroutine("updateCounterAfterPause");
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator updateCounterAfterPause()
    {
        yield return new WaitForSeconds(_delayToNextTick);
        _countdownTimer--;
        if (_countdownTimer > 0) {
            if (_audioSource && soundTick)
            {
                _audioSource.PlayOneShot(soundTick, 1.0f);
            }
            UpdateLabel(string.Format("{0}", _countdownTimer));
        }
        else if(_countdownTimer == 0)
        {
            if (_audioSource && soundGo)
            {
                _audioSource.PlayOneShot(soundGo, 1.0f);
            }
            _delayToNextTick = 2.0f;
            UpdateLabel("Go!");
        }
        else {
            LaunchGame();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameInterfaceCanvas : MonoBehaviour
{
    public Text _itemsCounterText;
    public Text _timerText;

    public GameInterfaceTimeDecrement gameInterfaceTimeDecrement;

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    void Start()
    {
        _itemsCounterText.text = "sfwqf";
        _timerText.text = "00:00";
    }

    // Update is called once per frame
    /***********************************************************
     * 
     **********************************************************/
    void Update()
    {
        _itemsCounterText.text = string.Format("{0}/{1}", GameManager.instance.eggsCaught, GameManager.instance.TargetItemsCount);

        int timeLeft = GameManager.instance.CountdownTime;
        int min = timeLeft / 60;
        int sec = timeLeft % 60;

        if (timeLeft > 15)
        {
            _timerText.color = Color.black;
        }
        else {
            _timerText.color = Color.red;
        }
        _timerText.text = string.Format("{0}:{1:D2}", min, sec);
    }

    /***********************************************************
    * 
    **********************************************************/
    public void TimeDecrementAnimation() {
        if (!_timerText) {
            return;
        }
        Vector3 worldPos = _timerText.transform.TransformPoint(Vector3.zero);

        GameInterfaceTimeDecrement newEffect = (GameInterfaceTimeDecrement)Instantiate(
                        gameInterfaceTimeDecrement,
                        worldPos,
                        Quaternion.identity);
        newEffect.transform.SetParent(_timerText.transform, true);
        newEffect.transform.position = worldPos;
    }

}

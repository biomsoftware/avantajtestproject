﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverLoseCanvas : GameOverCanvas
{
    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    void Start()
    {
        updateGameResultData();
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
    }

}

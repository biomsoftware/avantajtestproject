﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverCanvas : MonoBehaviour, IPointerClickHandler
{
    public Text EggsCounterText;
    public Text XScoresText;

    public int maxXScores = 50;
    public float delayBeforeXScores = 2.5f;

    private int _currentXScores = 0;
    private float _dXScores;

    public AudioClip soundOnShow;
    public AudioClip soundOnXScores;
    private AudioSource _audioSource;

    /***********************************************************
    * 
    **********************************************************/
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    /***********************************************************
     * 
     **********************************************************/
    void Start()
    {
        if (_audioSource && soundOnShow )
        {
            _audioSource.loop = false;
            _audioSource.PlayOneShot(soundOnShow, 1.0f);
        }
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        
    }

    public void updateGameResultData() {
        EggsCounterText.text = string.Format("{0}/{1}", GameManager.instance.eggsCaught, GameManager.instance.TargetItemsCount);

        _dXScores = 1.0f / maxXScores;
        if (XScoresText)
        {
            XScoresText.text = "";
            StartCoroutine("waitForXScores");
        }
    }

    /***********************************************************
     * 
     **********************************************************/
    public void OnPointerClick(PointerEventData eventData)
    {
        GoToMainMenu();
    }

    /***********************************************************
    * 
    **********************************************************/
    void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator waitForXScores()
    {
        yield return new WaitForSeconds(delayBeforeXScores);

        if (_audioSource && soundOnXScores)
        {
            _audioSource.PlayOneShot(soundOnXScores, 1.0f);
        }

        StartCoroutine("runForXScores");
    }

    /***********************************************************
     * 
     **********************************************************/
    IEnumerator runForXScores()
    {
        yield return new WaitForSeconds(_dXScores);
        _currentXScores++;
        if (XScoresText)
        {
            XScoresText.text = string.Format("x{0}", _currentXScores);
        }
        if (_currentXScores < maxXScores)
        {
            StartCoroutine("runForXScores");
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBasketContainer : MonoBehaviour
{
    public GameBasket gameBasket;

    public float leftLimit = -5.0f;
    public float rightLimit = 5.0f;

    public float basketSpeed = 5.0f;

    int shakeCounter;
    public float shakeDuration = .5f;
    public float shakeIntensity = .3f;

    private Vector3 _originPosition;
    private bool _isMouseDown = false;
    private bool _isPlaying = false;

    /***********************************************************
     * 
     **********************************************************/
    public void SetIsPlaying(bool value)
    {
        _isPlaying = value;
    }

    /***********************************************************
     * 
     **********************************************************/
    public void Show() {
        gameBasket.gameObject.SetActive(true); ;
    }

    /***********************************************************
    * 
    **********************************************************/
    public void Hide()
    {
        gameBasket.gameObject.SetActive(false); ;
    }

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    void Start()
    {
        gameBasket.enabled = false;
        _originPosition = transform.position;
        shakeCounter = 0;
        SetIsPlaying(false);
    }

    // Update is called once per frame
    /***********************************************************
     * 
     **********************************************************/
    void Update()
    {
        if (!_isPlaying) {
            return;
        }
        if (Input.touchCount > 0)
        {
            Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            Vector3 touchPosLocal = this.transform.InverseTransformPoint(touchPosWorld);

            if (Input.GetTouch(0).phase == TouchPhase.Began )
            {
                onTouchBegin(touchPosLocal);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                onTouchMove(touchPosLocal);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled || Input.GetMouseButtonUp(0))
            {
                onTouchEnd(touchPosLocal);
            }
        }
        else {
            Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 touchPosLocal = this.transform.InverseTransformPoint(touchPosWorld);

            if (Input.GetMouseButton(0) )
            {
                if (!_isMouseDown)
                {
                    _isMouseDown = true;
                    onTouchBegin(touchPosLocal);
                }
                else {
                    onTouchMove(touchPosLocal);
                }
            }
            if (!Input.GetMouseButton(0) && _isMouseDown)
            {
                _isMouseDown = false;
                onTouchEnd(touchPosLocal);
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveBasket(-basketSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveBasket(basketSpeed * Time.deltaTime);
        }

        if (shakeCounter > 0)
        {
            transform.position = _originPosition + Random.insideUnitSphere * shakeIntensity;
        }
        else {
            transform.position = _originPosition;
        }
    }

    /***********************************************************
     * 
     **********************************************************/
    void onTouchBegin(Vector3 touchPosLocal)
    {
        float basketX = gameBasket.transform.position.x;
        float newOffset = basketSpeed * Time.deltaTime;
        if (touchPosLocal.x > (basketX+ newOffset)) {
            moveBasket(newOffset);
        } else if (touchPosLocal.x < (basketX- newOffset))
        {
            moveBasket(-newOffset);
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    void onTouchMove(Vector3 touchPosLocal)
    {
        float basketX = gameBasket.transform.position.x;
        float newOffset = basketSpeed * Time.deltaTime;
        if (touchPosLocal.x > (basketX+ newOffset))
        {
            moveBasket(newOffset);
        }
        else if (touchPosLocal.x < (basketX- newOffset))
        {
            moveBasket(-newOffset);
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    void onTouchEnd(Vector3 touchPosLocal)
    {
    }

    /***********************************************************
    * 
    **********************************************************/
    void moveBasket(float offsetPosX) {
        float basketX = gameBasket.transform.position.x;
        float newX = basketX + offsetPosX;

        if (newX < leftLimit) {
            newX = leftLimit;
        }
        if (newX > rightLimit)
        {
            newX = rightLimit;
        }

        gameBasket.transform.position = new Vector3( newX, gameBasket.transform.position.y, gameBasket.transform.position.z);
    }

    /***********************************************************
    * 
    **********************************************************/
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Vector3 topPoint =
          new Vector3(leftLimit, this.transform.position.y, this.transform.position.z);
        Vector3 bottomPoint =
          new Vector3(rightLimit, this.transform.position.y, this.transform.position.z);
        Gizmos.DrawLine(topPoint, bottomPoint);
    }

    /***********************************************************
    * 
    **********************************************************/
    public void Shake() {
        if (shakeCounter < 2)
        {
            shakeCounter++;
        }
        if (shakeCounter < 2)
        {
            StartCoroutine("shakeCounterDec");
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator shakeCounterDec()
    {
        yield return new WaitForSeconds(shakeDuration);
        if (shakeCounter > 0)
        {
            shakeCounter--;
            StartCoroutine("shakeCounterDec");
        }
    }
}

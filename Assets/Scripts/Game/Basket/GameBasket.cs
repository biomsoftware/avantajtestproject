﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBasket : MonoBehaviour
{

    public GameBasketContainer gameBasketContainer;

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    void Start()
    {
        
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        
    }

    /***********************************************************
    * 
    **********************************************************/
    void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject objectThatHit = collider.gameObject;

        ItemPrototype objectItemPrototype = objectThatHit.transform.parent.GetComponent<ItemPrototype>();

        objectItemPrototype.hitWithObject(this.gameObject);
        gameBasketContainer.Shake();
    }
}

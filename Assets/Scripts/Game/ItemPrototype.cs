﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemPrototype : MonoBehaviour
{
    public enum ObjectType
    {
        Egg,
        Bomb
    }

    public enum SpeedType
    {
        Normal,
        Fast
    }

    public ItemPrefab eggBlue;
    public ItemPrefab eggRed;
    public ItemPrefab eggGreen;
    public ItemPrefab bombBlack;

    public ItemEffectLostEgg itemEffectLostEgg;
    public ItemEffectExplosionBomb itemEffectExplosionBomb;

    public float speedMultiplier = 2.0f;

    private Animator _itemAnimator;

    ItemPrefab _itemObject;
    private float _currentSpeed = 0.0f;
    private ObjectType _objectType = ObjectType.Egg;
    private SpeedType _speedType = SpeedType.Normal;

    private float _baseSpeed = 1.0f;
    private float _fastSpeedDelta = 1.0f;
    private float _bombSpeed = 4.0f;
    private bool _isPlaying = false;

    public ObjectType Object
    {
        get { return _objectType; }
        set
        {
            _objectType = value;
            switch (_objectType)
            {
                case ObjectType.Egg:
                    _objectType = ObjectType.Egg;
                    this.tag = "Egg";
                    break;
                case ObjectType.Bomb:
                    _objectType = ObjectType.Bomb;
                    this.tag = "Bomb";
                    break;
            }
        }
    }

    public SpeedType speedType
    {
        get { return _speedType; }
        set
        {
            _speedType = value;
            if (_objectType == ObjectType.Egg)
            {
                switch (_speedType)
                {
                    case SpeedType.Normal:
                        _currentSpeed = _baseSpeed;
                        break;
                    case SpeedType.Fast:
                        _currentSpeed = _baseSpeed + _fastSpeedDelta;
                        break;
                }
            }
            else if (_objectType == ObjectType.Bomb)
            {
                _currentSpeed = _bombSpeed;
            }
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    public bool isPlaying
    {
        get { return _isPlaying; }
        set
        {
           _isPlaying = value;
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    public void SetBaseSpeed(float value) {
        _baseSpeed = value;
        speedType = _speedType;

    }

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    void Start()
    {
        ItemPrefab itemPrefab = null;

        switch (_objectType) {
            case ObjectType.Bomb:
                itemPrefab = bombBlack;
                break;
            default:
                List<ItemPrefab> eggPrefabList = new List<ItemPrefab>();
                eggPrefabList.Add(eggBlue);
                eggPrefabList.Add(eggRed);
                eggPrefabList.Add(eggGreen);

                int randEggColor = Random.Range(0, eggPrefabList.Count);

                itemPrefab = eggPrefabList[randEggColor];
                break;
        }
        if (itemPrefab) {
            _itemObject = (ItemPrefab)Instantiate(
                            itemPrefab,
                            new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
                            Quaternion.identity);
        }
        if (_itemObject)
        {
            _itemObject.transform.SetParent(this.transform, true);
            _itemAnimator = _itemObject.GetComponent<Animator>();
            _itemAnimator.enabled = false;
        }
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        if (!_isPlaying) {
            return;
        }
        float newY = transform.position.y - ((_currentSpeed * speedMultiplier) * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
    }

    /***********************************************************
    * 
    **********************************************************/
    public void hitWithObject(GameObject objectThatHit)
    {
        if (objectThatHit.CompareTag("Basket"))
        {
            switch (_objectType)
            {
                case ObjectType.Bomb:
                    bombInBasket();
                    break;
                default:
                    eggInBasket();
                    break;
            }

        }
           if (objectThatHit.CompareTag("BottomTrap"))
        {
            switch (_objectType)
            {
                case ObjectType.Bomb:
                    bombInTrap();
                    break;
                default:
                    eggInTrap();
                    break;
            }
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    public void eggInBasket( )
    {
        ItemPrototype obj = this.GetComponent<ItemPrototype>();
        obj.OnCaught();
        GameManager.instance.ItemInBasket(obj.GetComponent<ItemPrototype>());
        this.isPlaying = false;
        DestroyWithAnimation();
    }

    /***********************************************************
    * 
    **********************************************************/
    public void bombInBasket()
    {
        ItemEffectExplosionBomb newEffect = (ItemEffectExplosionBomb)Instantiate(
                        itemEffectExplosionBomb,
                        new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
                        Quaternion.identity);
        newEffect.transform.SetParent(this.transform.parent, true);
        newEffect.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);

        ItemPrototype obj = this.GetComponent<ItemPrototype>();
        obj.OnCaught();
        GameManager.instance.ItemInBasket(obj.GetComponent<ItemPrototype>());
        this.isPlaying = false;
        DestroyWithAnimation();
    }

    /***********************************************************
     * 
     **********************************************************/
    public void eggInTrap()
    {
        ItemEffectLostEgg newEffect = (ItemEffectLostEgg)Instantiate(
                        itemEffectLostEgg,
                        new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z),
                        Quaternion.identity);
        newEffect.transform.SetParent(this.transform.parent, true);
        newEffect.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);

        ItemPrototype obj = this.GetComponent<ItemPrototype>();
        obj.OnTrap();
        GameManager.instance.ItemInTrap(obj.GetComponent<ItemPrototype>());
        this.isPlaying = false;
        DestroyWithAnimation();
    }

    /***********************************************************
     * 
     **********************************************************/
    public void bombInTrap()
    {
        ItemPrototype obj = this.GetComponent<ItemPrototype>();
        obj.OnTrap();
        GameManager.instance.ItemInTrap(obj.GetComponent<ItemPrototype>());
        this.isPlaying = false;
        DestroyWithAnimation();
    }

    /***********************************************************
    * 
    **********************************************************/
    public virtual void OnCaught()
    {
        _itemObject.OnCaught();
    }

    /***********************************************************
    * 
    **********************************************************/
    public virtual void OnTrap()
    {
        _itemObject.OnTrap();
    }

    /***********************************************************
     * 
     **********************************************************/
    public void DestroyWithAnimation() {
        if (_itemAnimator) { 
        _itemAnimator.enabled = true;
        }
        StartCoroutine("coroutineDestroyAfterPause");
    }

    /***********************************************************
     * 
     **********************************************************/
    IEnumerator coroutineDestroyAfterPause()
    {
        yield return new WaitForSeconds(0.25f);
        Destroy(this.gameObject);
    }

}

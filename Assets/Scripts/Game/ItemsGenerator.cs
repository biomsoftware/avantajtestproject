﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsGenerator : MonoBehaviour
{
    public GameObject itemPrefab;
    public float leftLimit = -5.0f;
    public float rightLimit = 5.0f;
    public int colsCount = 6;

    Dictionary<ItemPrototype.ObjectType, int> _activeItemsObjects = new Dictionary<ItemPrototype.ObjectType, int>();
    Dictionary<ItemPrototype.SpeedType, int> _activeItemsSpeed = new Dictionary<ItemPrototype.SpeedType, int>();

    public float itemSpeed = 1.0f;

    public int minActiveItems = 10;
    public int maxActiveItems = 20;

    public int minActiveFastItems = 1;
    public int maxActiveFastItems = 2;

    public int minBombsItems = 1;
    public int maxBombsItems = 2;

    public float bombProbability = 0.0f;

    private bool _isPlaying = false;
    private int _lastUsedCol = 0;
    List<GameObject> _activeItemsList = new List<GameObject>();

    /***********************************************************
    * 
    **********************************************************/
    public bool isPlaying
    {
        get { return _isPlaying; }
        set
        {
            if (value == true && !_isPlaying)
            {
                _isPlaying = value;
                createNewEgg();
            }
            _isPlaying = value;
        }
    }

    // Start is called before the first frame update
    /***********************************************************
     * 
     **********************************************************/
    void Start()
    {
        _activeItemsList = new List<GameObject>();
        ResetFieldData();

        createNewEgg();
    }

    /***********************************************************
    * 
    **********************************************************/
    void ResetFieldData() {
        _activeItemsObjects[ItemPrototype.ObjectType.Egg] = 0;
        _activeItemsObjects[ItemPrototype.ObjectType.Bomb] = 0;
        _activeItemsSpeed[ItemPrototype.SpeedType.Normal] = 0;
        _activeItemsSpeed[ItemPrototype.SpeedType.Fast] = 0;
        _activeItemsList.RemoveRange(0, _activeItemsList.Count);
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        
    }

    /***********************************************************
    * 
    **********************************************************/
    public void RemoveItem(ItemPrototype item) {
        ItemPrototype.SpeedType speedType = item.speedType;
        ItemPrototype.ObjectType objectType = item.Object;
        _activeItemsSpeed[speedType] -= 1;
        _activeItemsObjects[objectType] -= 1;

        int indexOfObject = _activeItemsList.IndexOf(item.gameObject);
        if (indexOfObject > -1)
        {
            _activeItemsList.RemoveAt(indexOfObject);
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    public void RemoveAllItems() {
        foreach (GameObject item in _activeItemsList)
        {
            item.GetComponent<ItemPrototype>().DestroyWithAnimation();
        }
        ResetFieldData();
    }

    /***********************************************************
    * 
    **********************************************************/
    void createNewEgg() {
        if (!_isPlaying)
        {
            return;
        }

        int randItemsLimit = Random.Range(minActiveItems, maxActiveItems+1);
        int randFastItemsLimit = Random.Range(minActiveFastItems, maxActiveFastItems+1);
        int totalActiveItems = _activeItemsSpeed[ItemPrototype.SpeedType.Normal] + _activeItemsSpeed[ItemPrototype.SpeedType.Fast];

        if (totalActiveItems < randItemsLimit)
        {
            ItemPrototype.ObjectType objectType = getObjectTypeForNextItem();

            ItemPrototype.SpeedType speedType = ItemPrototype.SpeedType.Normal;
            if (_activeItemsSpeed[ItemPrototype.SpeedType.Fast]< randFastItemsLimit) {
                speedType = ItemPrototype.SpeedType.Fast;
            }

            int colId = getColumnForNewItem();
            _lastUsedCol = colId;

            float colWidth = (rightLimit - leftLimit) / colsCount;

            GameObject newItem = (GameObject)Instantiate(
                itemPrefab,
                new Vector3(leftLimit + colWidth * colId + colWidth * 0.5f, this.transform.position.y, this.transform.position.z),
                Quaternion.identity);

            newItem.GetComponent<ItemPrototype>().Object = objectType;
            newItem.GetComponent<ItemPrototype>().SetBaseSpeed(itemSpeed);
            newItem.GetComponent<ItemPrototype>().speedType = speedType;
            newItem.GetComponent<ItemPrototype>().isPlaying = true;

            newItem.transform.SetParent(this.transform, true);
            _activeItemsSpeed[speedType] += 1;
            _activeItemsObjects[objectType] += 1;
            _activeItemsList.Add(newItem);

            GameManager.instance.AddGeneratedEgg();
        }

         StartCoroutine("createNewEggAfterPause");
    }

    /***********************************************************
     * 
     **********************************************************/
    ItemPrototype.ObjectType getObjectTypeForNextItem() {
        ItemPrototype.ObjectType result = ItemPrototype.ObjectType.Egg;
        int bombsActiveItems = _activeItemsObjects[ItemPrototype.ObjectType.Bomb];
        bool requireBomb = false;

        if (bombProbability > 0)
        {
            if (bombsActiveItems < minBombsItems)
            {
                requireBomb = true;
            }
            if (bombsActiveItems >= maxBombsItems)
            {
                requireBomb = false;
            }
            if (requireBomb) {
                float rand = Random.Range(0, 1.0f);
                if (rand > bombProbability) {
                    requireBomb = false;
                }
            }
        }

        if (requireBomb)
        {
            result = ItemPrototype.ObjectType.Bomb;
        }
        return result;
    }

    /***********************************************************
    * 
    **********************************************************/
    int getColumnForNewItem() {
        if (colsCount<2) {
            return 0;
        }
        int result = 0;
        result = Random.Range(0, colsCount);
        if (_lastUsedCol == result)
        {
            result = getColumnForNewItem();
        }
        if (Mathf.Abs(_lastUsedCol-result)>2) {
            result = getColumnForNewItem();
        }
        return result;
    }

    /***********************************************************
    * 
    **********************************************************/
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Vector3 topPoint =
          new Vector3(leftLimit, this.transform.position.y, this.transform.position.z);
        Vector3 bottomPoint =
          new Vector3(rightLimit, this.transform.position.y, this.transform.position.z);
        Gizmos.DrawLine(topPoint, bottomPoint);
    }


    /***********************************************************
    * 
    **********************************************************/
    IEnumerator createNewEggAfterPause()
    {
        yield return new WaitForSeconds(0.2f);
        createNewEgg();
    }

}


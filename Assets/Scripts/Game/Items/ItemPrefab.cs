﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPrefab : MonoBehaviour
{
    public AudioClip soundOnCaught;
    public AudioClip soundOnTrap;

    private AudioSource _audioSource;

    // Start is called before the first frame update
    /***********************************************************
    * 
    **********************************************************/
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        
    }

    /***********************************************************
    * 
    **********************************************************/
    public virtual void OnCaught() {
        playSoundOnCaught();
    }

    /***********************************************************
    * 
    **********************************************************/
    public virtual void OnTrap()
    {
        playSoundOnTrap();
    }

    /***********************************************************
    * 
    **********************************************************/
    public void playSoundOnCaught() {
        if (_audioSource && soundOnCaught) {
            _audioSource.PlayOneShot(soundOnCaught, 1.0f);
        }
    }

    /***********************************************************
    * 
    **********************************************************/
    public void playSoundOnTrap()
    {
        _audioSource = GetComponent<AudioSource>();
        if (_audioSource && soundOnTrap)
        {
            _audioSource.PlayOneShot(soundOnTrap, 1.0f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEffect : MonoBehaviour
{
    public float delayBeforeAutodestroy = 1.0f;

    // Start is called before the first frame update
    /***********************************************************
     * 
     **********************************************************/
    void Start()
    {
        StartCoroutine("delayedAutodestroy");
    }

    // Update is called once per frame
    /***********************************************************
    * 
    **********************************************************/
    void Update()
    {
        
    }

    /***********************************************************
    * 
    **********************************************************/
    IEnumerator delayedAutodestroy()
    {
        yield return new WaitForSeconds(delayBeforeAutodestroy);
        Destroy(this.gameObject);
    }

}

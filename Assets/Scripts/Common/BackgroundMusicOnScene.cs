﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicOnScene : Singleton<BackgroundMusicOnScene>
{
    private AudioSource _audioSource;
    public AudioClip backgroundMusic;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = 0.5f;
        Stop();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Stop()
    {
        _audioSource.Stop();
    }

    public void Pause()
    {
        _audioSource.Pause();
    }

    public void Play()
    {
        _audioSource.Play();
    }
}

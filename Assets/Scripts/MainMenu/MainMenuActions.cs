﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuActions : MonoBehaviour
{
    public AudioClip soundPlayButton;

    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        BackgroundMusicOnScene.instance.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void buttonPlayOnClick() {
        _audioSource.PlayOneShot(soundPlayButton, 1.0f);
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }
}
